package com.itinerant.game.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.itinerant.game.PacmanGame;
import com.itinerant.game.screens.BaseScreen;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.width = BaseScreen.WIDTH;
		config.height = BaseScreen.HEIGHT;
		new LwjglApplication(new PacmanGame(), config);
	}
}
