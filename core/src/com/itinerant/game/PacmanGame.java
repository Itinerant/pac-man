package com.itinerant.game;

import com.badlogic.gdx.Game;
import com.itinerant.game.screens.GameScreen;

/**
 * Created by roman on 01.08.18.
 */

public class PacmanGame extends Game {

    @Override
    public void create() {
        setScreen(new GameScreen());
    }
}
