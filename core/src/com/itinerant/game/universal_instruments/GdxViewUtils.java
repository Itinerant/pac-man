package com.itinerant.game.universal_instruments;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

/**
 * Created by roman on 01.08.18.
 */

public class GdxViewUtils {

    public static ImageButton createImageButton(Texture idleTexture, Texture activeTexture){
        TextureRegionDrawable idleDrawable  = new TextureRegionDrawable(new TextureRegion(idleTexture));
        TextureRegionDrawable activeDrawable = new TextureRegionDrawable(new TextureRegion(activeTexture));
        ImageButton button = new ImageButton(idleDrawable, activeDrawable);
        return button;
    }
}
