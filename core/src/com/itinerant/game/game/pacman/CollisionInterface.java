package com.itinerant.game.game.pacman;

/**
 * Created by roman on 09.08.18.
 */

public interface CollisionInterface {

    boolean isCollision(float pacmanX, float pacmanY, float pacmanW, float pacmanH);
}
