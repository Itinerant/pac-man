package com.itinerant.game.game.pacman;

import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Align;
import com.itinerant.game.game.GameViewElement;

/**
 * Created by roman on 02.08.18.
 */

public class PacmanView extends GameViewElement {

    private PacmanModel model;
    private PacmanController controller;
    private Image pacmanImage;

    public PacmanView(PacmanModel model, PacmanController controller) {
        this.model = model;
        model.setListener(this);
        this.controller = controller;
        sourceFolder = "pacman/";
        createComponents();
        initSize();
        this.setOrigin(Align.center);
    }

    private void initSize() {
        this.setSize(pacmanImage.getWidth(), pacmanImage.getHeight());
    }

    private void createComponents() {
        createPacmanImage();
    }

    private void createPacmanImage() {
        pacmanImage = new Image(getTexture("pacman"));
        addActor(pacmanImage);
    }
}
