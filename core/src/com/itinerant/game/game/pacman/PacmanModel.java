package com.itinerant.game.game.pacman;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by roman on 02.08.18.
 */

public class PacmanModel {

    private PacmanView listener;

    private float rotation;
    private float pixelsPerSecond;  //pixels per second
    private float xFactor;
    private float yFactor;
    private float rotateDuration = 0.1f;

    private List<CollisionInterface> collisionInterfaces;

    public PacmanModel() {
        pixelsPerSecond = 400f;
        rotation = 90f;
        collisionInterfaces = new LinkedList<CollisionInterface>();
        updateRotation();
    }

    public void addCollisionListener(CollisionInterface collisionListener) {
        collisionInterfaces.add(collisionListener);
    }

    public void setListener(PacmanView listener) {
        this.listener = listener;
    }

    //region interface

    public void up() {
        rotation = 90f;
        xFactor = 0f;
        yFactor = 1;
        updateRotation();
    }

    public void down() {
        rotation = 270f;
        xFactor = 0f;
        yFactor = -1;
        updateRotation();
    }

    public void left() {
        rotation = 180f;
        xFactor = -1f;
        yFactor = 0;
        updateRotation();
    }

    public void right() {
        rotation = 0f;
        xFactor = 1f;
        yFactor = 0;
        updateRotation();
    }

    public void update(float delta) {
        if (listener != null) {
            float xAmplitude = xFactor * pixelsPerSecond * delta;
            float yAmplitude = yFactor * pixelsPerSecond * delta;
            float x = listener.getX() + xAmplitude;
            float y = listener.getY() + yAmplitude;
            if (isInBounds(x, y) && !isAnyCollision(x, y)) {
                listener.setPosition(x, y);
            }
        }
    }

    //endregion

    //region logic

    private boolean isAnyCollision(float x, float y) {
        float w = listener.getWidth();
        float h = listener.getHeight();
        boolean result = false;
        for (CollisionInterface collisionInterface : collisionInterfaces){
            result &= collisionInterface.isCollision(x, y, w, h);
        }
        return result;
    }

    private boolean isInBounds(float x, float y) {
        float w = listener.getWidth();
        float h = listener.getHeight();
        float boundsW = Gdx.graphics.getWidth();
        float boundsH = Gdx.graphics.getHeight();
        boolean isXInBounds = 0 <= x && x + w <= boundsW;
        boolean isYInBounds = 0 <= y && y + h <= boundsH;
        return isXInBounds && isYInBounds;
    }

    private void updateRotation() {
        if (listener != null) {
            listener.clearActions();
            listener.addAction(Actions.rotateTo(rotation, rotateDuration));
        }
    }

    //endregion
}
