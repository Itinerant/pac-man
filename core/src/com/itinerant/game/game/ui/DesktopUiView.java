package com.itinerant.game.game.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.itinerant.game.game.GameViewElement;

/**
 * Created by roman on 01.08.18.
 */

class DesktopUiView extends GameViewElement {

    private UIController controller;

    public DesktopUiView(UIController controller) {
        this.controller = controller;
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        if (Gdx.input.isKeyJustPressed(Input.Keys.W)) {
            controller.up();
        } else if (Gdx.input.isKeyJustPressed(Input.Keys.S)) {
            controller.down();
        } else if (Gdx.input.isKeyJustPressed(Input.Keys.A)) {
            controller.left();
        } else if (Gdx.input.isKeyJustPressed(Input.Keys.D)) {
            controller.right();
        }
    }
}
