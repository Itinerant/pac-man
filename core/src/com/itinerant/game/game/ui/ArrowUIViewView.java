package com.itinerant.game.game.ui;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Disposable;
import com.itinerant.game.game.GameViewElement;
import com.itinerant.game.universal_instruments.GdxViewUtils;

/**
 * Created by roman on 01.08.18.
 */

class ArrowUIViewView extends GameViewElement implements Disposable {

    private UIController controller;

    private ImageButton btnUp;
    private ImageButton btnDown;
    private ImageButton btnLeft;
    private ImageButton btnRight;

    public ArrowUIViewView(UIController controller) {
        this.controller = controller;
        sourceFolder = "ui/";
        createComponents();
        calculateSize();
        for (Actor actor : getChildren()) {
            actor.setDebug(true);
        }
    }

    //region initialization

    private void calculateSize() {
        float btnHeight = btnDown.getHeight();
        float btnWidth = btnDown.getWidth();
        setSize(btnWidth * 3f, btnHeight * 2f);
    }

    private void createComponents() {
        createBtnUp();
        createBtnDown();
        createBtnLeft();
        createBtnRight();
        calculateButtonsPositions();
    }

    private void calculateButtonsPositions() {
        float btnHeight = btnDown.getHeight();
        float btnWidth = btnDown.getWidth();
        btnDown.setX(btnWidth);
        btnRight.setX(2f * btnWidth);
        btnUp.setPosition(btnWidth, btnHeight);
    }

    private void createBtnRight() {
        btnRight = GdxViewUtils.createImageButton(getTexture("arrow_right"), getTexture("arrow_right"));
        btnRight.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                controller.right();
            }
        });
        addActor(btnRight);
    }

    private void createBtnLeft() {
        btnLeft = GdxViewUtils.createImageButton(getTexture("arrow_left"), getTexture("arrow_left"));
        btnLeft.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                controller.left();
            }
        });
        addActor(btnLeft);
    }

    private void createBtnDown() {
        btnDown = GdxViewUtils.createImageButton(getTexture("arrow_down"), getTexture("arrow_down"));
        btnDown.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                controller.down();
            }
        });
        addActor(btnDown);
    }

    private void createBtnUp() {
        btnUp = GdxViewUtils.createImageButton(getTexture("arrow_up"), getTexture("arrow_up"));
        btnUp.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                controller.up();
            }
        });
        addActor(btnUp);
    }

    //endregion

    //region ui_view_interface

    //endregion

    @Override
    public void dispose() {
        clear();
        if (controller != null) {
            controller = null;
        }
    }
}
