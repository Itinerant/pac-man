package com.itinerant.game.game.ui;

/**
 * Created by roman on 01.08.18.
 */

public class UIController {

    private UIModel model;

    public UIController(UIModel model) {
        this.model = model;
    }

    //region interface

    public void up() {
        model.up();
    }

    public void down() {
        model.down();
    }

    public void left() {
        model.left();
    }

    public void right() {
        model.right();
    }

    //endregion
}
