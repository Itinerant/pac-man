package com.itinerant.game.game.ui;

import com.itinerant.game.game.pacman.PacmanModel;

/**
 * Created by roman on 01.08.18.
 */

public class UIModel {

    private PacmanModel move;

    public UIModel(PacmanModel move) {
        this.move = move;
    }

    ///region interface

    public void up() {
        move.up();
    }

    public void down() {
        move.down();
    }

    public void left() {
        move.left();
    }

    public void right() {
        move.right();
    }

    //endregion
}

