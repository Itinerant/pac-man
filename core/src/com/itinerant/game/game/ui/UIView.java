package com.itinerant.game.game.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.itinerant.game.game.GameViewElement;

/**
 * Created by roman on 02.08.18.
 */

public class UIView extends GameViewElement {

    private UIController controller;

    public UIView(UIController controller) {
        this.controller = controller;
        initView();
    }

    private void initView() {
        Group platformUI = null;
        switch (Gdx.app.getType()) {
            case Desktop:
                platformUI = new DesktopUiView(controller);
                break;
            case iOS:
            case Android:
                platformUI = new ArrowUIViewView(controller);
                break;
        }
        if (platformUI != null) {
            addActor(platformUI);
        } else {
            Gdx.app.error("UIView", "initView: platformUI is null");
        }
    }
}
