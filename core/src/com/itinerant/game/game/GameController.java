package com.itinerant.game.game;

import com.itinerant.game.game.pacman.PacmanController;
import com.itinerant.game.game.ui.UIController;

/**
 * Created by roman on 02.08.18.
 */

public class GameController {

    private GameModel model;
    private PacmanController pacmanController;
    private UIController uiController;

    public GameController(GameModel model) {
        this.model = model;
        pacmanController = new PacmanController(model.getPacmanModel());
        uiController = new UIController(model.getUiModel());
    }

    public PacmanController getPacmanController() {
        return pacmanController;
    }

    public UIController getUiController() {
        return uiController;
    }
}
