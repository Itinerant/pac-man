package com.itinerant.game.game;

import com.itinerant.game.game.pacman.PacmanView;
import com.itinerant.game.game.ui.UIView;

/**
 * Created by roman on 02.08.18.
 */

public class GameView extends GameViewElement {

    private GameModel model;
    private GameController controller;

    private PacmanView pacmanView;
    private UIView uiView;

    public GameView(GameModel model, GameController controller) {
        this.model = model;
        this.controller = controller;
        createCompotents();
    }

    //region initialization

    private void createCompotents() {
        createPacman();
        createUI();
    }

    private void createUI() {
        uiView = new UIView(controller.getUiController());
    }

    private void createPacman() {
        pacmanView = new PacmanView(model.getPacmanModel(), controller.getPacmanController());
    }

    //endregion

    public PacmanView getPacmanView() {
        return pacmanView;
    }

    public UIView getUiView() {
        return uiView;
    }
}
