package com.itinerant.game.game.map;

import com.itinerant.game.game.pacman.CollisionInterface;

/**
 * Created by roman on 09.08.18.
 */

public class MapElementModel implements CollisionInterface {

    private MapElementView listener;

    private float x;
    private float y;
    private float w;
    private float h;

    public MapElementModel() {
    }

    public void setListener(MapElementView listener) {
        this.listener = listener;
    }

    @Override
    public boolean isCollision(float pacmanX, float pacmanY, float pacmanW, float pacmanH) {
        if (listener != null) {

        }
        return false;
    }
}
