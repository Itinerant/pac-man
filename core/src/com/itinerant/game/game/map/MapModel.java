package com.itinerant.game.game.map;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by roman on 09.08.18.
 */

public class MapModel {

    private MapView listener;
    private List<MapElementModel> elementModels;

    public MapModel() {
        elementModels = new LinkedList<MapElementModel>();
    }

    public void setListener(MapView listener) {
        this.listener = listener;
    }

    public List<MapElementModel> getElementModels() {
        return elementModels;
    }
}
