package com.itinerant.game.game.map;

import com.badlogic.gdx.scenes.scene2d.Group;

/**
 * Created by roman on 09.08.18.
 */

public class MapElementView extends Group {

    private MapElementModel model;

    public MapElementView(MapElementModel model) {
        this.model = model;
    }
}
