package com.itinerant.game.game.map;

import com.badlogic.gdx.scenes.scene2d.Group;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by roman on 09.08.18.
 */

public class MapView extends Group {

    private MapModel model;
    private List<MapElementView> elementViews;

    public MapView(MapModel model) {
        this.model = model;
        model.setListener(this);
        elementViews = new LinkedList<MapElementView>();
        createElements();
    }

    private void createElements() {
        for (MapElementModel elementModel : model.getElementModels()) {
            MapElementView elementView = new MapElementView(elementModel);
            addActor(elementView);
        }
    }
}
