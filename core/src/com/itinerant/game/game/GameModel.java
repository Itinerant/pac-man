package com.itinerant.game.game;

import com.itinerant.game.game.pacman.PacmanModel;
import com.itinerant.game.game.ui.UIModel;

/**
 * Created by roman on 02.08.18.
 */

public class GameModel {

    private PacmanModel pacmanModel;
    private UIModel uiModel;

    public GameModel() {
        pacmanModel = new PacmanModel();
        uiModel = new UIModel(pacmanModel);
    }

    //region getters

    public PacmanModel getPacmanModel() {
        return pacmanModel;
    }

    public UIModel getUiModel() {
        return uiModel;
    }

    //endregion

    public void update(float delta) {
        if (pacmanModel != null) {
            pacmanModel.update(delta);
        }
    }
}
