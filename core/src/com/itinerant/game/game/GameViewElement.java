package com.itinerant.game.game;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.utils.StringBuilder;

/**
 * Created by roman on 01.08.18.
 */

public class GameViewElement extends Group {

    protected String sourceFolder = "";
    protected String subFolder = "";

    protected Texture getTexture(String name) {
        StringBuilder sb = new StringBuilder();
        sb.append(sourceFolder);
        sb.append(subFolder);
        sb.append(name);
        sb.append(".png");
        return new Texture(sb.toString());
    }
}
