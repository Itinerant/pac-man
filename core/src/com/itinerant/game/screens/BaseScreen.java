package com.itinerant.game.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.utils.viewport.FitViewport;

/**
 * Created by roman on 01.08.18.
 */

public abstract class BaseScreen implements Screen {

    public Stage stage;
    public SpriteBatch batch;
    public OrthographicCamera camera;
    public FitViewport viewport;

    public static int WIDTH = 1280;
    public static int HEIGHT = 720;

    private Group untouchableGroup;
    private Group uiGroup;

    @Override
    public void show() {
        reset();
        camera = new OrthographicCamera();
        camera.update();
        viewport = new FitViewport(WIDTH, HEIGHT, camera);
        batch = new SpriteBatch();
        stage = new Stage(viewport, batch);
        untouchableGroup = new Group();
        untouchableGroup.setTouchable(Touchable.disabled);
        stage.addActor(untouchableGroup);
        uiGroup = new Group();
        stage.addActor(uiGroup);

        InputMultiplexer multiplexer = new InputMultiplexer(stage);
        Gdx.input.setInputProcessor(multiplexer);

        start();

        Gdx.gl.glClearColor(1.f, 1.f, 1.f, 1);
    }

    public abstract void start();

    public void reset() { //TODO is it important?
        camera = null;
        if (batch != null) {
            batch.dispose();
            batch = null;
        }
        if (stage != null) {
            stage.dispose();
            stage = null;
        }
    }

    @Override
    public void render(float delta) {
        try {
            camera.update();
            batch.setProjectionMatrix(camera.combined);
            Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);
            stage.act(delta);
            viewport.update(WIDTH, HEIGHT);
            update(delta);
            draw();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void addUntouchableActor(Actor actor){
        untouchableGroup.addActor(actor);
    }

    public void addUIActor(Actor actor) {
        uiGroup.addActor(actor);
    }

    @Override
    public void resize(int width, int height) {
        viewport.update(width, height);
    }

    public void draw() {
        if (stage != null) {
            stage.draw();
        }
    }

    public abstract void update(float delta);

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {
        dispose();
    }

    @Override
    public void dispose() {
        if (stage != null) {
            stage.dispose();
            stage = null;
        }
    }
}
