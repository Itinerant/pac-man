package com.itinerant.game.screens;

import com.itinerant.game.game.GameController;
import com.itinerant.game.game.GameModel;
import com.itinerant.game.game.GameView;

/**
 * Created by roman on 01.08.18.
 */

public class GameScreen extends BaseScreen {

    private GameModel model;
    private GameController controller;
    private GameView view;

    @Override
    public void start() {
        model = new GameModel();
        controller = new GameController(model);
        view = new GameView(model, controller);
        addUIActor(view.getUiView());
        addUntouchableActor(view.getPacmanView());
    }

    @Override
    public void update(float delta) {
        model.update(delta);
    }
}
